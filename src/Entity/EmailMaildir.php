<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

use App\Entity\Traits\EmailTrait;
use App\Helper\EmailHelper;

/**
 * @ORM\Table(name="maildir")
 * @ORM\Entity(repositoryClass="App\Repository\EmailsRepository")
 */
class EmailMaildir
{
    use EmailTrait;

    /**
     * Non-persisted field for the home information.
     *
     * @var string $home
     */
    protected $home;

    /**
     * @return string
     */
    public function getHome(): string
    {
        return EmailHelper::emailToMaildir(strToLower($this->getEmail()));
    }
}



