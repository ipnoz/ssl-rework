<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

use App\Entity\Traits\EmailTrait;
use App\Entity\Traits\EmailAliasTrait;

/**
 * @ORM\Table(name="filterAlias")
 * @ORM\Entity(repositoryClass="App\Repository\EmailsRepository")
 */
class EmailFilterAlias
{
    use EmailTrait, EmailAliasTrait;
}



