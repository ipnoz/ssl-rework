<?php

declare(strict_types=1);

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait EmailAliasTrait
{
    /**
     * @var string $forward
     *
     * @ORM\Column
     */
    protected $forward;


    /**
     * @return string|null
     */
    public function getForward(): ?string
    {
        return $this->forward;
    }

    /**
     * @param string $forward
     */
    public function setForward(string $forward)
    {
        $this->forward = strToLower($forward);
    }
}
