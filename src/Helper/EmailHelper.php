<?php

declare(strict_types=1);

namespace App\Helper;

class EmailHelper
{
    /**
     * @param string $email
     *
     * @return string
     */
    public static function emailToMaildir(string $email): string
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \InvalidArgumentException($email.' is not a valid email');
        }
        list($user, $domain) = explode('@', $email);
        return $domain.'/'.$user;
    }
}
