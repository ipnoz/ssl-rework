<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\EmailAlias;
use App\Entity\EmailFilterAlias;
use App\Entity\EmailMaildir;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class DefaultController extends AbstractController
{
    /**
     * @param string $type
     *
     * @return array
     * @throws \InvalidArgumentException
     */
    private function getEmailTypeInfos(string $type): array
    {
        switch ($type) {
            case 'maildirs':
                $class = EmailMaildir::class;
                $title = 'Maildirs';
                break;
            case 'aliases':
                $class = EmailAlias::class;
                $title = 'Aliases';
                break;
            case 'filters':
                $class = EmailFilterAlias::class;
                $title = 'Filter aliases';
                break;
            default:
                throw new \InvalidArgumentException($type.' is not a valid email type');
        }

        return ['className' => $class, 'title' => $title, 'type' => $type];
    }

    /**
     * @Route("/", name="index")
     *
     * @return Response
     */
    public function indexAction(): Response
    {
        return $this->render('index.html.twig');
    }

    /**
     * @Route("/emails/{type}", name="dashboard_emails")
     *
     * @param EntityManagerInterface $entityManager
     *
     * @return Response
     */
    public function dashboardEmailsAction(
        EntityManagerInterface $entityManager,
        Request $request,
        string $type = 'filters'
    ): Response {

        $datas = $this->getEmailTypeInfos($type);

        $datas['list'] = $entityManager->getRepository($datas['className'])->findAllArrayAssoc();

        if ($request->isXmlHttpRequest()) {
            $return = $this->json($datas);
        } else {
            $return = $this->render('list_emails.html.twig', $datas);
        }

        return $return;
    }

    /**
     * @Route("/email/{type}/toggle_state/{id}", name="email_toggle_state")
     *
     * @return JsonResponse
     */
    public function toggleEmailStateAction(
        EntityManagerInterface $entityManager,
        string $type,
        int $id
    ): JsonResponse {

        $datas = $this->getEmailTypeInfos($type);

        $email = $entityManager->getRepository($datas['className'])->find($id);

        if (!is_object($email)) {
            return $this->json([
                'status' => 'error',
                'message' => 'email id '.$id.' not found'
            ], 400);
        }

        $email->setIsEnabled(!$email->getIsEnabled());
        $entityManager->flush();

        return $this->json(['status' => 'success']);
    }
}
