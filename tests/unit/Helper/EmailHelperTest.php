<?php

declare(strict_types=1);

namespace App\Tests\Unit\Helper;

use App\Helper\EmailHelper;
use Codeception\Test\Unit;

class EmailHelperTest extends Unit
{
    public function test_email_to_maildir_require_a_valid_email(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        EmailHelper::emailToMaildir('invalidEmail');
    }

    public function test_email_to_maildir_return_maildir_structure(): void
    {
        $this->assertSame(
            'domain.tld/firstName.lastName',
            EmailHelper::emailToMaildir('firstName.lastName@domain.tld')
        );
    }
}
