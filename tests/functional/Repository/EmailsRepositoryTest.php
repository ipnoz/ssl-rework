<?php

declare(strict_types=1);

namespace App\Tests\Functional\Repository;

use App\Entity\EmailAlias;
use App\Entity\EmailFilterAlias;
use App\Entity\EmailMaildir;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class EmailsRepositoryTest extends KernelTestCase
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * {@inheritDoc}
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->em = $kernel->getContainer()->get('doctrine')->getManager();
    }

    public function test_repository_methodfor_email_maildir_entity(): void
    {
        $list = $this->em->getRepository(EmailMaildir::class)->findAllArrayAssoc();

        $this->assertIsArray($list);
        $this->assertCount(3, $list);
    }

    public function test_repository_methodfor_email_filter_alias_entity(): void
    {
        $list = $this->em->getRepository(EmailAlias::class)->findAllArrayAssoc();

        $this->assertIsArray($list);
        $this->assertCount(26, $list);
    }

    public function test_repository_methodfor_email_alias_entity(): void
    {
        $list = $this->em->getRepository(EmailFilterAlias::class)->findAllArrayAssoc();

        $this->assertIsArray($list);
        $this->assertCount(181, $list);
    }


    /**
     * {@inheritDoc}
     */
    protected function tearDown(): void
    {
        parent::tearDown();

        $this->em->close();
        // avoid memory leaks
        $this->em = null;
    }
}
