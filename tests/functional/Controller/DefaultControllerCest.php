<?php

declare(strict_types=1);

namespace App\Tests\Functional\Controller;

use App\Tests\FunctionalTester;

class DefaultControllerCest
{
    private function seeHeaders(FunctionalTester $I)
    {
        $I->seeLink('Home', '/');
        $I->seeLink('Maildirs', '/emails/maildirs');
        $I->seeLink('Aliases', '/emails/aliases');
        $I->seeLink('Filters', '/emails');
    }

    public function get_index(FunctionalTester $I): void
    {
        $I->amOnPage('/');
        $I->seeResponseCodeIs(200);
        $I->see('Administration');
        $this->seeHeaders($I);
    }

    public function get_filter_aliases_table_as_default_type_in_emails_dashboard_page(FunctionalTester $I): void
    {
        $I->amOnPage('/emails');
        $I->seeResponseCodeIs(200);
        $this->seeHeaders($I);
        $I->see('Filter aliases');
        $I->seeElement('table');
    }

    public function get_maildirs_type_table_in_emails_dashboard_page(FunctionalTester $I): void
    {
        $I->amOnPage('/emails/maildirs');
        $I->seeResponseCodeIs(200);
        $this->seeHeaders($I);
        $I->see('Maildirs');
        $I->seeElement('table');
    }

    public function get_aliases_type_table_in_emails_dashboard_page(FunctionalTester $I): void
    {
        $I->amOnPage('/emails/aliases');
        $I->seeResponseCodeIs(200);
        $this->seeHeaders($I);
        $I->see('Aliases');
        $I->seeElement('table');
    }

    public function invalid_email_type_throw_exception(FunctionalTester $I): void
    {
        $I->amOnPage('/emails/invalidType');
        $I->seeResponseCodeIs(500);
        $I->see('invalidType is not a valid email type');
    }

    public function toggle_email_state(FunctionalTester $I): void
    {
        $id = 4;
        $table = 'filterAlias';

        $I->updateInDatabase($table, ['is_enabled' => 1], ['id' => $id]);

        $I->amOnPage('/email/filters/toggle_state/'.$id);
        $I->seeResponseCodeIs(200);
        $I->seeInDatabase($table, ['id' => $id, 'is_enabled' => 0]);

        $I->amOnPage('/email/filters/toggle_state/'.$id);
        $I->seeResponseCodeIs(200);
        $I->seeInDatabase($table, ['id' => $id, 'is_enabled' => 1]);
    }

    public function toggle_email_state_with_invalid_type_throw_exception(FunctionalTester $I): void
    {
        $id = 4;
        $table = 'filterAlias';

        $I->updateInDatabase($table, ['is_enabled' => 1], ['id' => $id]);

        $I->amOnPage('/email/invalidType/toggle_state/'.$id);
        $I->seeResponseCodeIs(500);
        $I->see('invalidType is not a valid email type');

        $I->dontSeeInDatabase($table, ['id' => $id, 'is_enabled' => 0]);
    }

    public function toggle_email_state_with_invalid_email_return_error(FunctionalTester $I): void
    {
        $id = 999999999999;
        $table = 'filterAlias';

        $I->updateInDatabase($table, ['is_enabled' => 1], ['id' => $id]);

        $I->amOnPage('/email/filters/toggle_state/'.$id);
        $I->seeResponseCodeIs(400);
        $I->see('email id '.$id.' not found');

        $I->dontSeeInDatabase($table, ['id' => $id, 'is_enabled' => 0]);
    }
}
