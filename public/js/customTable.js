
(function($) {

    'use strict';

    let table = $('table.dataTable').DataTable({
        paging: false,
        responsive: true,
        columnDefs: [
            { 'targets': 0, 'orderData': [ 4, 0 ] },
            { 'targets': 1, 'orderData': [ 4, 1 ] },
            { 'targets': 2, 'orderData': [ 4, 2 ] },
            { 'targets': 3, 'orderData': [ 4, 3 ] },
            {
                targets: 4,
                'createdCell': function (td, cellData, rowData, row, col) {
                    displayEmailStateOnTable($(td));
                }
            }
        ]
    });

    // Default order
    table.order([ 4, 'desc' ], [ 0, 'asc' ]).draw();

    function displayEmailStateOnTable(td) {
        if (td.data('order') === 1) {
            td.closest('tr').removeClass('alert-warning');
            td.find('i').removeClass('fas fa-trash-restore-alt');
            td.find('i').addClass('fas fa-trash-alt');
        } else {
            td.closest('tr').addClass('alert-warning');
            td.find('i').removeClass('fas fa-trash-restore-alt');
            td.find('i').addClass('fas fa-trash-restore-alt');
        }
    }

    $('a.toggle-email-state').on({
        click: function(e) {

            e.preventDefault();

            let td = $(this).closest('td');

            $.ajax({
                url: $(this).attr('href'),
                type: 'get',
                dataType: 'json',
                success: function(response) {
                    if (response.status === 'success') {
                        if (td.data('order') === 1) {
                            td.data('order', 0);
                        } else {
                            td.data('order', 1);
                        }
                        displayEmailStateOnTable(td);
                    }
                }
            });
        }
    });

})(jQuery);